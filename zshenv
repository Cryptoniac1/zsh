
export PATH="$PATH:/home/lucas/.local/bin:/home/lucas/.local/share/flatpak/exports/bin/"
export CM_SELECTIONS="clipboard"
export CM_DEBUG=0
export CM_MAX_CLIPS=25
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export QT_QPA_PLATFORMTHEME=qt6ct

export CACA_DRIVER=ncurses

export EDITOR=vim
#export GTK_IM_MODULE=ibus
#export QT_IM_MODULE=ibus
#export XMODIFIERS=@im=ibus
export HISTFILESIZE=10000
export HISTCONTROL=ignoreboth
export STARDICT_DATA_DIR=~/dict/

# AUTOCOMPLETE
export ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE="20"
export ZSH_AUTOSUGGEST_COMPLETION_IGNORE="ssh *"

#if [[ "$(tty)" = "/dev/tty1" ]]; then
#	startx
#	/usr/bin/dbus-daemon --session --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
#fi
