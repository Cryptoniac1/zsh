# Lines configured by zsh-newuser-install
git="git@gitlab.com:Cryptoniac1"
setopt autocd extendedglob notify auto_pushd
unsetopt BEEP

bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/lucas/.zshrc'
zstyle ':completion:*' hosts off
autoload -U colors
colors
#PROMPT="%F{red}[%f%B%F{red}%n%f%b@%F{46}%m%f%F{red}:%f%F{blue}%~%f%F{red}]%f$ "


# History 
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=10000
setopt HIST_SAVE_NO_DUPS
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
zmodload zsh/complist

## Keybindings
bindkey "jj" vi-cmd-mode
bindkey "^p" insert-last-word
bindkey "^l" clear-screen
bindkey "^k" reverse-menu-complete
bindkey "^w" which-command
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey '^n' autosuggest-accept

## Aliases ##
# Music
alias weeb="mpv --vid=no --shuffle --no-cookies --quiet --save-position-on-quit 'https://www.youtube.com/playlist?list=PLvM17dhWlAUFc4r7egpHw9-mrQm2DfqhJ'"
alias inst="mpv --vid=no --shuffle --no-cookies --quiet --save-position-on-quit 'https://www.youtube.com/playlist?list=PLvM17dhWlAUG-_vShsaTgk3-7iersN_9X'"
alias ytmdl="~/scripts/ytmdl.sh"
alias ec="env WAYLAND_DISPLAY=$(echo WAYlAND_DISPLAY) tmux a -t suite"

# Navigation
alias :q='exit'
alias ..='cd ..' 
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# Options
alias locate='locate --ignore-case'
alias grep='grep --color=auto'
alias w3m="w3m -no-cookie"
alias vifm="~/.config/vifm/scripts/vifmrun"

# Quick Actions
alias e="gpg -r lucas@carrcrypt.com -e"
alias d="gpg -d"
alias p.='python'
alias ll="ls -lah"
alias la="ls -a"
alias Yc="sudo pacman -Qqdt | sudo pacman -Rns -"
alias YC="sudo pacman -Qqdt | sudo pacman -Rns -"
alias thesaurus="sdcv -u English\ Thesaurus"
alias scripts="git push $git/scripts"
alias weather="curl wttr.in/napa"
alias psync="rsync -razPvhc --delete ~/Music ~/mnt/Internal\ shared\ storage"
alias m3u="ls | grep -v list.m3u | grep -v ௸  | grep -v saved_vids > list.m3u && echo ௸  >> list.m3u"
alias escan="~/scripts/easy_scan.sh"

# Quick Edits
alias i3e="vim ~/.config/i3/config"
alias brc="vim ~/.bashrc; . ~/.bashrc"
alias vrc="vim ~/.vimrc"
alias zrc="vim ~/.zshrc; source ~/.zshrc"
alias zsrc="vim ~/.zshrc; source ~/.zshrc"

#Screen
alias mkscrn="screen -S"
alias lscrn="screen -ls"
alias r="screen -r"

# Scripts
alias backup="~/scripts/backup.sh"
alias mkdoc="~/scripts/mkdoc.sh"
alias yt-cli="~/scripts/yt_cli.sh"
alias yt="~/scripts/yt-search.sh"
alias search="~/scripts/search.sh"
alias wiki="~/scripts/wikipedia -p"
alias wifi="~/scripts/wifi.sh"

# SSH
alias linode="ssh -i /home/lucas/.ssh/linode_rsa lucas@23.239.0.225"
alias crypt="ssh -Y 10.69.0.227"
alias dns="ssh 10.69.0.10"

# Trolls
alias rr="curl -s -L http://bit.ly/10hA8iC | bash"
alias suod="curl -s -L http://bit.ly/10hA8iC | bash  ; #"


## End Aliases ##

## Functions
function vpdf { pdftotext -layout "$1" - | less; }

#other settings

if [[ -z $TMUX ]] && [[ -n $SSH_TTY ]]; then exec tmux new-session -A -s ssh; fi
figlet $(cat /etc/hostname) | lolcat && neofetch | lolcat
#cowsay $(fortune)

function preexec() {
  timer=${timer:-$SECONDS}
}

function precmd() {
  if [ $timer ]; then
    timer_show=$(($SECONDS - $timer))
    export RPROMPT="%F{red}${timer_show}s %{$reset_color%}"
    unset timer
  fi
}
eval "$(starship init zsh)"

# Plugins

source ~/.config/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.config/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
